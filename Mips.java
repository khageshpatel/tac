
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

class Register {
    public Boolean dirty;
    public String  label;
	public Boolean writeProtected,assigned;
	public Register(){
		dirty = writeProtected = assigned = Boolean.FALSE;
	}
}

class Pair {
	
	public int start,end;
	public Pair(int a, int b){
		start = a;
		end   = b;
	}
	
}

public class Mips{

	Vector<Vector<String>> instructions;
	String dataSection;
	String textSection;
	
	String currentFunction;
	Map<String,Integer> inRegisters;
	Map<String,Boolean> isGlobal;
	
	public Map<String,Pair> FuncToIntervalMap;
	Map<String,Boolean> memoryAssigned;
	
	Register[] registers;
	
	int PC;
	int stringLableCounter;
	
	String libraryFunctions;
	
   public Mips(){
      instructions = new Vector<Vector<String>>();
	  registers    = new Register[32];
	  for(int i=0;i<32;i++)
		  registers[i] = new Register();
	  registers[0].writeProtected = registers[1].writeProtected = registers[26].writeProtected = registers[27].writeProtected = 
	  registers[28].writeProtected = registers[29].writeProtected = registers[30].writeProtected = Boolean.TRUE;
	  
	  registers[2].writeProtected = Boolean.TRUE; //$v0 is reserved as it is frequently used for returing value and some intermediate operation
	  
	  inRegisters = new HashMap<String,Integer>();
	  isGlobal    = new HashMap<String,Boolean>();;
	  
	  FuncToIntervalMap = new HashMap<String,Pair>();
	  memoryAssigned = new HashMap<String,Boolean>();
	  
	  try{
		  libraryFunctions = new String(Files.readAllBytes(Paths.get("library.s")));
	  }
	  catch (Exception e){
			System.out.println("Some error occured while reading library function");
			System.out.println(e.getMessage()); 
	  }
	  
	  PC = 0;
	  
	  stringLableCounter = 1;
	  
	  dataSection  = ".data\n";
	  textSection  = ".text\n.globl main\nmain:\n";
   }

   String VarToLabel(String v){
	   String str = "_" + currentFunction + "_" + v + "_";
	   if(isGlobal.containsKey(str) && isGlobal.get(str) == Boolean.TRUE)
		   return "_global_" + v + "_";
	   else
		   return str;
   }
   
   int VariableToRegister(String var){   //Impleentation of Linear Scan Register Allocation Algorithm by M. Poletto & V. Sarkar
	   String Label = VarToLabel(var);
	   if(inRegisters.containsKey(Label) && inRegisters.get(Label) != -1)  // Check if already mapped to some register
		   return inRegisters.get(Label);		
	   
	   for(int i=0;i<32;i++) //See if we can find a free register
		   if(registers[i].writeProtected == Boolean.FALSE && registers[i].assigned == Boolean.FALSE)
		   {
			   textSection = textSection + "lw $" + i + ", "  +  Label + "\n";
			   registers[i].label = Label;
			   registers[i].assigned = Boolean.TRUE;
			   inRegisters.put(Label, i);
			   return i;
		   }
		   
	   for(int i=0;i<32;i++) //See if we can find a register not belonging to current block
		   if(registers[i].writeProtected == Boolean.FALSE && registers[i].label.startsWith("_" + currentFunction + "_"))
		   {
			   SpillRegister(i);
			   textSection = textSection + "lw $" + i + ", "  +  Label + "\n";
			   registers[i].label = Label;
			   registers[i].assigned = Boolean.TRUE;
			   inRegisters.put(Label, i);
			   return i;
		   }
		
	   for(int i=0;i<32;i++) //See if we can find a register with variable whose scope has ended
		   if(registers[i].writeProtected == Boolean.FALSE && FuncToIntervalMap.get(registers[i].label).end<PC)
		   {
			   SpillRegister(i);
			   textSection = textSection + "lw $" + i + ", "  +  Label + "\n";
			   registers[i].label = Label;
			   registers[i].assigned = Boolean.TRUE;
			   inRegisters.put(Label, i);
			   return i;
		   }
	   
	   int maxIndex = -1;
	   for(int i=0;i<32;i++) //If nothing works select one according to linear scan algorithm
		   if(registers[i].writeProtected == Boolean.FALSE)
		   {
			   if(maxIndex == -1 || FuncToIntervalMap.get(registers[i].label).end > FuncToIntervalMap.get(registers[maxIndex].label).end)
				   maxIndex = i;
		   }
		SpillRegister(maxIndex);
		textSection = textSection + "lw $" + maxIndex + ", "  +  Label + "\n";
		registers[maxIndex].label = Label;
		registers[maxIndex].assigned = Boolean.TRUE;
		inRegisters.put(Label,maxIndex);
		return 0;
   }
   
   void SpillRegister(int i){
	   registers[i].dirty = registers[i].assigned  = Boolean.FALSE;
	   textSection = textSection + "sw $" + i + ", "  +  registers[i].label + "\n";
	   inRegisters.put(registers[i].label, -1);
   }
   
   boolean isVariable(String str){
	   if(str.equals("+") || str.equals("-") || str.equals("*") || str.equals("/") || str.equals("%") || 
	      str.equals("=") || str.equals("&&") || str.equals("||") || str.equals("<") || str.equals("==") ||
		  str.matches("goto|IfZ|If|<|==|PushParameter|PopParameter|call|BeginFunc|EndFunc|return|global|Array"))
		  return false; //Stabdard reserved words in TAC
	   if( str.charAt(str.length() - 1) == ':')
		   return false; // Labels in TAC
	   if(str.matches("[-+]?\\d*\\.?\\d+") || str.startsWith("\"")) //Check if numeric or a string
		   return false;
	   return true;
   }
   
   void AssignmentInstr(Vector<String> instruction){
	   int source = VariableToRegister(instruction.get(1));
	   registers[source].dirty = Boolean.TRUE;
	   
	   String srcLabel = VarToLabel(instruction.get(1));
	   if(!memoryAssigned.containsKey(srcLabel) || memoryAssigned.get(srcLabel) == Boolean.FALSE)  // Assign global memory space for source variable
	   {
		   memoryAssigned.put(srcLabel,Boolean.TRUE);
		   dataSection = dataSection + srcLabel + ": .word 0\n";
	   }
		   
	   if(isVariable(instruction.get(2)))
	   {
		   String dstLabel = VarToLabel(instruction.get(2));
		   if(!memoryAssigned.containsKey(dstLabel) || memoryAssigned.get(dstLabel) == Boolean.FALSE)  // Assign global memory space destination variable
	       {
		      memoryAssigned.put(dstLabel,Boolean.TRUE);
		      dataSection = dataSection + dstLabel + ": .word 0\n";
	       }
	   
		   int destination = VariableToRegister(instruction.get(2));
		   textSection = textSection + "move $" + source + ", $"  +  destination + "\n";
	   }
	   else if(instruction.get(2).matches("[-+]?\\d*\\.?\\d+"))
	   {
		   textSection = textSection + "li $" + source + ", "  +  Integer.parseInt(instruction.get(2)) + "\n";
	   }
	   else if(instruction.get(2).startsWith("\"")){
		   dataSection = dataSection + "_string_" + stringLableCounter + ": .asciiz " + instruction.get(2) + "\n";
		   textSection = textSection + "la $" + source + ", "  + "_string_" + stringLableCounter + "\n";
		   stringLableCounter = stringLableCounter + 1;
	   }
   }
   
   void CallInstruction(Vector<String> instruction){
	   String funcName = instruction.get(1);
	   if(!funcName.matches("_halt|_PrintString|_print_int"))
		   funcName = "_func_" + funcName;
	   int noArguments = Integer.parseInt(instruction.get(2));
	   boolean returnValue = false;
	   String storeLocation = "";
	   if(instruction.size()>3){
		   returnValue = true;
		   storeLocation = instruction.get(3);
	   }
	   
	   textSection = textSection + "jal " + funcName + "\n";
	   if(noArguments != 0)
		   textSection = textSection + "la $sp, " + (noArguments*4) + "($sp)\n";
	   if(returnValue){
		   String dstLabel = VarToLabel(storeLocation);
		   if(!memoryAssigned.containsKey(dstLabel) || memoryAssigned.get(dstLabel) == Boolean.FALSE)  // Assign global memory space destination variable
	       {
		      memoryAssigned.put(dstLabel,Boolean.TRUE);
		      dataSection = dataSection + dstLabel + ": .word 0\n";
	       }
		   int destination = VariableToRegister(storeLocation);
		   textSection = textSection + "move $" + destination + ", $v0\n";
	   }
   }
   
   void PushInstruction(Vector<String> instruction){
	   String var = instruction.get(1);
	   String varLabel = VarToLabel(var);
		   if(!memoryAssigned.containsKey(varLabel) || memoryAssigned.get(varLabel) == Boolean.FALSE)  // Assign global memory space for variable
	       {
		      memoryAssigned.put(varLabel,Boolean.TRUE);
		      dataSection = dataSection + varLabel + ": .word 0\n";
	       }
	   int location = VariableToRegister(var);
	   textSection = textSection + "sw $" + location + ", -4($sp)\n";
	   textSection = textSection + "la $sp, -4($sp)\n";
   }
   
   public void GenerateAssembly(){
	   int noOfInstr = instructions.size();
	   for(int i=0;i<noOfInstr;i++)
	   {
		   Vector<String> instruction = instructions.get(i);
		   
		   if(instruction.get(0).equals("="))
			   AssignmentInstr(instruction);
		   else if(instruction.get(0).equals("call"))
				CallInstruction(instruction);
		   else if(instruction.get(0).equals("PushParameter"))
				PushInstruction(instruction);
			
		   PC = PC + 1;
	   }
	   
	   try(PrintWriter out = new PrintWriter( "machine.s" )){
				out.println(dataSection);
				out.println(textSection + libraryFunctions);
		}
		catch (Exception e){
			System.out.println("Some error occured while writing output file");
			System.out.println(e.getMessage()); 
		}
	   
   }
   
   public static void main(String []args){
	  Mips Generator = new Mips(); 
	  int flag = 0;
	  Vector<Vector<String>> tmpInstr = new Vector<Vector<String>>();
      try (BufferedReader br = new BufferedReader(new FileReader(args[0]))) {
			String line;
			while ((line = br.readLine()) != null) {
				Vector<String> instruction = new Vector<String>();
				Matcher m = Pattern.compile("([^\"]\\S*|\".+?\")\\s*").matcher(line);
				while (m.find())
					instruction.add(m.group(1).trim());
				
				if(instruction.size()==0)
					continue;
				
				if(instruction.firstElement().equals("BeginFunc"))
					flag = 1;
				if(flag == 0)
					Generator.instructions.add(instruction);
				else
					tmpInstr.add(instruction);
				if(instruction.firstElement().equals("EndFunc"))
					flag = 0;
			}
			Generator.instructions.addAll(tmpInstr);
			flag = Generator.instructions.size();
			
			Generator.currentFunction = "global";
			
			for(int i=0;i<flag;i++){
				int opCodes = Generator.instructions.get(i).size();
				if(Generator.instructions.get(i).get(0).equals("BeginFunc")){
					Generator.currentFunction = Generator.instructions.get(i).get(1);
					continue;
				}
				for(int j = 0;j<opCodes;j++)
				{
					if(Generator.isVariable(Generator.instructions.get(i).get(j)))
					{
						String Label = Generator.VarToLabel(Generator.instructions.get(i).get(j));
						if(!Generator.FuncToIntervalMap.containsKey(Label)){
							Generator.FuncToIntervalMap.put(Label,new Pair(i,i));
							continue;
						}
						Generator.FuncToIntervalMap.get(Label).end = i;
					}
						//System.out.println(Generator.instructions.get(i).get(j));
				}
			}
			
			Generator.currentFunction = "global";
			
			Generator.GenerateAssembly();
		}
	  catch (Exception e){
			System.out.println("Some error occured while reading input file");
			System.out.println(e.getMessage()); 
		}
		
   }
}