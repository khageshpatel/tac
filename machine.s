.data
_global_a_: .word 0
_global_b_: .word 0
_global_str_: .word 0
_string_1: .asciiz "Khagesh"

.text
.globl main
main:
lw $3, _global_a_
li $3, 4
lw $4, _global_b_
li $4, 5
lw $5, _global_str_
la $5, _string_1
sw $3, -4($sp)
la $sp, -4($sp)
jal _print_int
la $sp, 4($sp)
jal _halt
_halt: #Exit
	addi	$v0, $zero, 10
	syscall
	
_print_int:
	li $v0, 1
	sw $a0, -4($sp)
	lw $a0, 0($sp)
	syscall
	lw $a0, -4($sp)
	jr $ra
