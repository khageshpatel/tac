_halt: #Exit
	addi	$v0, $zero, 10
	syscall
	
_print_int:
	li $v0, 1
	sw $a0, -4($sp)
	lw $a0, 0($sp)
	syscall
	lw $a0, -4($sp)
	jr $ra